require 'activeresource'

module Blip
  class Connection < ActiveResource::Connection
    def initialize(*a)
      super *a
      @default_header = { 'Content-Type' => format.mime_type, 'Accept' => format.mime_type, 'X-Blip-API' => '0.02' }
    end
  end
  
  class Resource < ActiveResource::Base
    self.site = 'http://api.blip.pl'
    self.format = :json

    class << self
      def connection(refresh = false)
        @connection = Connection.new(site, format) if refresh || @connection.nil?
        @connection.user = user if user
        @connection.password = password if password
        @connection.timeout = timeout if timeout
        @connection
      end
      def const_missing name
        const_set name, Class.new(Resource) unless const_defined? name
        const_get name
      end
    
    end
    
    def method_missing sym, *args
      begin
        super
      rescue NoMethodError
        name = sym.to_s
        if @attributes.include?(name + '_path')
          klass = Blip.const_get name.camelcase
          klass.find(:one, :from => @attributes[name + '_path'])
        end
      end
    end

    def update
      returning connection.put(stripped_path(prefix_options), attributes.to_json, self.class.headers) do |response|
        load_attributes_from_response(response)
      end
    end

    # Create (i.e., save to the remote service) the new resource.
    def create
      returning connection.post(stripped_path, attributes.to_json, self.class.headers) do |response|
        self.id = id_from_response(response)
        load_attributes_from_response(response)
      end
    end
    
    def stripped_path(*a) # strips back out the extension
      element_path(*a).sub(/\/?\.#{self.class.format.extension}/, "")
    end

  end
  
  class << self
    def const_missing name
      const_set name, Class.new(Resource) unless const_defined? name
      const_get name
    end
    
    def method_missing name, *args
      Resource.send name, args
    end
  end
end
