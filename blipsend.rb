#!/usr/bin/env ruby

require 'blip'

Blip.user, Blip.password = ARGV[0].split(':')

Blip::Update.create(:body => STDIN.read)
